import UIKit

//Task 3.
//3.1. Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі величини.
//3.2 Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі парні величини,
//а потів всі непарні значення.

var myArray:[Int] = [1,2,3,4,5,6,7,8,9]

for value in myArray {
    print(value, terminator: " ")
}

print("")
var i = 0
while i < myArray.count {
    print(myArray[i], terminator: " ")
    i += 1
}

print("")
var zz = 0
repeat {
    print(myArray[zz], terminator: " ")
    zz += 1
} while zz < myArray.count


print("\nEven values:")
for value in myArray where value % 2 == 0 {
    print(value, terminator: " ")
}

print("")
var index = 0
while index < myArray.count {
    if myArray[index] % 2 == 0 {
        print(myArray[index], terminator: " ")
    }
    index += 1
}

print("")
var y = 0
repeat {
    if myArray[y] % 2 == 0 {
      print(myArray[y], terminator: " ")
    }
    y += 1
} while y < myArray.count


print("\nOdd values:")
for value in myArray where value % 2 != 0 {
   print(value, terminator: " ")
}

print("")
var x = 0
while x < myArray.count {
    if myArray[x] % 2 != 0 {
        print(myArray[x], terminator: " ")
    }
    x += 1
}

print("")
var z = 0
repeat {
    if myArray[z] % 2 != 0 {
        print(myArray[z], terminator: " ")
    }
    z += 1
} while z < myArray.count




