import UIKit

var w = 42
var y = 13
var z = 6

var avg = (w + y + z) / 3
print("Arithmetic mean: \(avg)")

print("Geometric mean:", pow(Double(w * y * z), (1.0/3.0)))

var a:Double = 4.0
var b:Double = 4.0
var c:Double = -8.0
var root1, root2, discriminant: Double

//a*x^2 + b*x + c = 0,

discriminant = pow(b, 2) - 4 * a * c
print("The discriminant is: \(discriminant)")

if discriminant == 0 {
    root1 = -b / (2 * a)
    print("Root1: \(root1)")
} else if discriminant < 0 {
    print("Does not have roots")
} else {
    root1 = (-b - sqrt(discriminant)) / (2 * a)
    print("Root 1: \(root1)")
    
    root2 = (-b + sqrt(discriminant)) / (2 * a)
    print("Root 2: \(root2)")
}
