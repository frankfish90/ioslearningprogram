import UIKit

//Task 2
//We have a 4 lists of students from one group.
//allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
//present on Monday = [1, 2, 5, 6, 7]
//present on Tuesday = [3, 6, 8, 10]
//present on Wednesday = [1, 3, 7, 9, 10]
//Print students that attend university three days, two days, Monday and Wednesday but not Tuesday,
//missed all classes

let allStudents1: Set = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let presentOnMonday1: Set = [1, 2, 5, 6, 7]
let presentOnTuesday1: Set = [3, 6, 8, 10]
let presentOnWednesday1: Set = [1, 3, 7, 9, 10]

print("Students attend university three days", presentOnMonday1.intersection(presentOnTuesday1).intersection(presentOnWednesday1).sorted())

print("Students that attend two days", presentOnMonday1.intersection(presentOnTuesday1).union(presentOnTuesday1.intersection(presentOnWednesday1).union(presentOnWednesday1.intersection(presentOnMonday1))).sorted())

print("Students attend Monday and Wednesday but not Tuesday", presentOnMonday1.intersection(presentOnWednesday1).subtracting(presentOnTuesday1).sorted())

print("Students that missed all classes", allStudents1.symmetricDifference(presentOnMonday1).intersection(allStudents1.symmetricDifference(presentOnTuesday1)).intersection(allStudents1.symmetricDifference(presentOnWednesday1)))

