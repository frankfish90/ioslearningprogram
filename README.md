
###[RD UA iOS] Online iOS External Program #2

Module 1. Self-Study. Source control basics (GIT), main commands, SourceTree
- repository purpose
- git/svn
- git repository creation, branching strategy
- git main commands (add, commit, push, pull)
Матеріали:
https://about.gitlab.com/get-started/

Module 2. Self-study. Foundation. Variable and types. Operators.
-constants and variables -type annotations -naming
-printing
-comments
-basic types (Int/Double/Float, Bool, String) -type safety/inference/conversion/aliases -literals
-tuples
Матеріали:
https://docs.swift.org/swift-book/LanguageGuide/TheBasics.html https://www.raywenderlich.com/5539282-programming-in-swift-fundamentals/lessons/1

Module 3. Self-study. Strings
- string literals (single- and multiline) - special characters
- empty string
- string mutability
- string concatenation - string interpolation - count, substrings
Матеріали:
https://docs.swift.org/swift-book/LanguageGuide/StringsAndCharacters.html

Module 4. Self-study. Basic Operators
- assignment
- "+", "-", "*", "/", "%", unary "-"    
- "+=", "-=", "*=", "/="
- "==", "!=", ">", "<", ">=", "<="
- "?:"
- "...", "..<", one-sided ranges
- "!", "&&", "||"
- combining, explicit parenthess, order of operations in complex flows

Матеріали:
https://docs.swift.org/swift-book/LanguageGuide/BasicOperators.html
Практичне завдання:
Оголосити декілька змінних типу Int обчисли: середнє арифметичне, геометричне.
Оголосити змінні для коефіцієнтів квадритного рівняння (a, b, c) типу Double, обчисли корені рівняння. Використовуючи функцію print() вивести корені у вигляді: "a*x^2 + b*x + c = 0, root1 =1 , root2=2 "